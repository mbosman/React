import React, { Component } from "react";
import "./App.css";
import {
  Button,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Drawer,
  List,
  ListItemText,
  Divider,
} from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import Login from "./pages/Login";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";

class App extends Component {
  state = {
    open: false,
    dir: "lft",
  };
  render() {
    const handleDrawerOpen = () => {
      this.setState({ open: true });
    };
    const handleDrawerClose = () => {
      this.setState({ open: false });
    };
    return (
      <div className="App">
        <Router>
          <div>
            <AppBar position="static">
              <Toolbar>
                <IconButton color="inherit" onClick={handleDrawerOpen}>
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap>
                  Website Name
                </Typography>
              </Toolbar>
            </AppBar>
            <Drawer variant="persistent" anchor="left" open={this.state.open}>
              <div>
                <IconButton onClick={handleDrawerClose}>
                  <ChevronLeftIcon />
                </IconButton>
              </div>
              <Divider />
              <List>
                <ListItemText>
                  <Button
                    className="ListButton"
                    onClick={handleDrawerClose}
                    component={Link}
                    to="/"
                  >
                    Home
                  </Button>
                </ListItemText>
                <listItem>
                  <ListItemText>
                    <Button
                      className="ListButton"
                      onClick={handleDrawerClose}
                      component={Link}
                      to="/login"
                    >
                      Login{" "}
                    </Button>
                  </ListItemText>
                </listItem>
                <listItem>
                  <ListItemText>
                    <Button
                      className="ListButton"
                      onClick={handleDrawerClose}
                      component={Link}
                      to="/users"
                    >
                      Users
                    </Button>
                  </ListItemText>
                </listItem>
              </List>
            </Drawer>{" "}
          </div>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/about">
              <h1>Test About</h1>
            </Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/" component={Dashboard}></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
