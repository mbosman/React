import React, { Component } from "react";
import axios from "axios";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  Paper,
  TableRow,
  TablePagination,
  CircularProgress,
  LinearProgress,
} from "@material-ui/core";
class Dashboard extends Component {
  state = {
    data1: [],
    page: 0,
    rowsPerPage: 10,
    hideLoading: false,
  };
  route = "http://192.168.2.101:5000/";
  componentDidMount() {
    this.getData();
  }
  async getData() {
    await axios.get(this.route).then((r) => {
      var data1 = r.data.recordset;
      this.setState({ data1 });
    });
    this.setState({ loading: true });
    console.log(this.state);
  }
  columns = [
    { id: "fname", label: "First Name" },
    { id: "lname", label: "Last Name" },
    { id: "email", label: "Email" },
  ];
  render() {
    const handleChangePage = (event, newPage) => {
      this.setState({ page: newPage });
    };

    const handleChangeRowsPerPage = (event) => {
      var val = +event.target.value;
      this.setState({ rowsPerPage: val });
      this.setState({ page: 0 });
    };
    return (
      <Container>
        <h1>Hello</h1>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                {this.columns.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.data1
                .slice(
                  this.state.page * this.state.rowsPerPage,
                  this.state.page * this.state.rowsPerPage +
                    this.state.rowsPerPage
                )
                .map((row) => {
                  return (
                    <TableRow hover tabIndex={-1} key={row.id}>
                      <TableCell component="th" scope="row">
                        {row.first_name}
                      </TableCell>
                      <TableCell>{row.last_name}</TableCell>
                      <TableCell>{row.email}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={this.state.data1.length}
          rowsPerPage={this.state.rowsPerPage}
          page={this.state.page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        <div hidden={this.state.loading}>
          <LinearProgress />
        </div>
      </Container>
    );
  }
}
export default Dashboard;
