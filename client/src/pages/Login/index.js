import React, { Component } from "react";
import {
  Container,
  TextField,
  Paper,
  FormControl,
  InputLabel,
  FormGroup,
  Button,
} from "@material-ui/core";
import axios from "axios";
class Login extends Component {
  state = {
    firstname: "",
    lastname: "",
  };
  route = "http://192.168.2.101:5000/";

  render() {
    const changeInput = (event) => {
      var name = event.target.value;
      this.setState({ firstname: name });
    };
    const changeInput2 = (event) => {
      var name = event.target.value;
      this.setState({ lastname: name });
    };
    const submitForm = () => {
      var values = this.state;
      axios
        .post(this.route, { values })
        .then(function (response) {
          console.log(response.data.values);
        })
        .catch(function (error) {
          console.log(error);
        });
    };
    return (
      <Container component={Paper}>
        <form>
          <FormGroup>
            <TextField
              id="first_name"
              label="First Name"
              onChange={changeInput}
            />
          </FormGroup>
          <FormGroup>
            <TextField
              id="last_name"
              label="Last Name"
              onChange={changeInput2}
            />
          </FormGroup>
          <FormGroup>
            <Button onClick={submitForm}>Submit</Button>
          </FormGroup>
        </form>
      </Container>
    );
  }
}
export default Login;
