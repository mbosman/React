const cors = require("cors");
const express = require("express");
const { getPool } = require("./database");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors());

//! MAKE SQL CALLS
async function runQuery(query) {
  const pool = await getPool("default");
  const result = await pool.request().query(query);
  var values = result;
  return values;
}
//! --------------------------------------------

app.get("/", async (req, res) => {
  let result = await runQuery("Select * From Home.Test");
  return res.send(result);
});
app.post("/", (req, res) => {
  console.log(req.body);
  return res.send(req.body);
});
app.listen(5000, () => console.log(`Example app listening on port 5000!`));
